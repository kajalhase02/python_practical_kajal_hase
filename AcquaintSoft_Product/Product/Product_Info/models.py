from django.db import models

# Create your models here.
product_choices = (
    ('x','x'),
    ('y','y'),
    ('z','z'),
)

category_choices = (
    ('p','p'),
    ('q','q'),
    ('r','r'),
    ('s','s'),

)
class Category(models.Model):
    prod_category = models.CharField(max_length=50,choices=category_choices)
    def __str__(self):
        return self.prod_category


class Product(models.Model):
    prod_name=models.CharField(max_length=20,choices=product_choices)
    prod_code=models.CharField(max_length=20)
    prod_price=models.PositiveIntegerField()
    manu_date=models.DateTimeField()
    exp_date=models.DateTimeField()
    prod_owner=models.CharField(max_length=20)
    Prod_status=models.CharField(max_length=20)
    prod_category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.prod_name




from django.contrib import admin
from .models import *


# Register your models here.

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['prod_category']
admin.site.register(Category, CategoryAdmin)

class ProductAdmin(admin.ModelAdmin):
    list_display = ['prod_name','prod_code','prod_price','manu_date','exp_date','prod_owner','Prod_status','prod_category']
admin.site.register(Product,ProductAdmin)
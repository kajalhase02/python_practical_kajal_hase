from django.apps import AppConfig


class ProductInfoConfig(AppConfig):
    name = 'Product_Info'

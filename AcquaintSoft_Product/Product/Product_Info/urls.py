from django.urls import path
from . import views
from .views import *

urlpatterns = [
    path('h/',home),
    path('a/',about),
    path('p/',policies),
    path('f/',feedback),
    path('c/',contact),
    path('addprod/',addprod),
    path('allprod/',allprod, name='allprod'),
    path('addcat/',addcategory),
    path('allcat/',allcategory),
    path('update/<int:pid>/', views.updateprod, name='update'),
    path('delete/<int:pid>/', views.deleteprod, name='delete'),
    path('reg/',regview, name='reg'),
    path('log/',loginview, name='log'),
    path('logout/',logoutview, name='logout1')

]
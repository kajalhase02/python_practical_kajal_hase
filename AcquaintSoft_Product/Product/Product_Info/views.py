from django.shortcuts import render,redirect

from django.http import HttpResponse
from .forms import *
from .models import Product
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
# Create your views here.
def home(request):

    template_name='Product_Info/home.html'
    return render(request,template_name)

def about(request):

    template_name='Product_Info/about.html'
    return render(request,template_name)

def policies(request):

    template_name='Product_Info/policies.html'
    return render(request,template_name)

def feedback(request):

    template_name='Product_Info/feedback.html'
    return render(request,template_name)

def contact(request):

    template_name='Product_Info/contact.html'
    return render(request,template_name)

@login_required
def addcategory(request):
    if request.method == 'POST':
        print("Post req..")
        form = CategoryForm(request.POST)
        if form.is_valid():
            print('Data is Valid..')
            form.save()
            return redirect('/product/allcat/')
    else:
        print("Get req..")
        catobj=CategoryForm()
        template_name="Product_Info/addcat.html"
        context={'form':catobj}
        return render(request, template_name,context)

@login_required
def allcategory(request):
    catobj = Category.objects.all()
    contex = {'cat': catobj}
    template_name="Product_Info/allcat.html"
    return render(request,template_name,contex)

@login_required
def addprod(request):
    if request.method == 'POST':
        print("Post req..")
        form = ProductForm(request.POST)
        if form.is_valid():
            print('Data is Valid..')
            form.save()
            return redirect('/product/allprod/')
    else:
        print("Get req..")
        prodobj=ProductForm()
        template_name="Product_Info/addprod.html"
        context={'form':prodobj}
        return render(request, template_name,context)

@login_required
def allprod(request):
    prodobj = Product.objects.all()
    contex = {'prod': prodobj}
    template_name="Product_Info/allprod.html"
    return render(request,template_name,contex)

@login_required
def updateprod(request,pid):
    prod = Product.objects.get(pk=pid)
    if request.method == 'GET':
        form = ProductForm(instance=prod)
    elif request.method == 'POST':
        form = ProductForm(request.POST, instance=prod)
        if form.is_valid():
            form.save()
            return redirect('/product/allprod/')
    contex = {'form': form}
    template_name = 'Product_Info/update_prod.html'
    return render(request,template_name,contex)

@login_required
def deleteprod(request,pid):
    prodobj = Product.objects.get(pk=pid)
    if request.method == 'GET':
        contex={'prod':prodobj}
        template_name = 'Product_Info/deleteprod.html'
        return render(request, template_name, contex)
    elif request.method == 'POST':
        prod = Product.objects.get(pk=pid)
        prod.delete()
        return redirect('/product/allprod/')

def regview(request):
    if request.method == 'GET':
        form = UserCreationForm()
        contex = {'form': form}
        template_name = 'Product_Info/reg.html'
        return render(request, template_name, contex)
    elif request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
           form.save()
           print("User Registered")
           template_name = 'Product_Info/log.html'
           return render(request, template_name)
        return render(request,'Product_Info/reg.html',{'form':form})

def loginview(request):
    if request.method == 'GET':
        contex={}
        template_name = 'Product_Info/log.html'
        return render(request, template_name, contex)
    elif request.method == 'POST':
        u = request.POST['uname']
        p = request.POST['pw']
        user = authenticate(username=u,password=p)
        if user is not None:
            login(request, user)
            next_url = request.GET.get('next')
            if next_url:
                return redirect(next_url)
            else:
                return redirect('allprod')
        else:
            contex = {}
            messages.error(request,'Invalid Credentials!')
            template_name = 'Product_Info/log.html'
            return render(request,template_name,contex)

def logoutview(request):
    logout(request)

    return redirect('log')









